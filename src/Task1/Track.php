<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
        $this->cars=array();
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        array_push($this->cars,$car);
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $bestCarIndex=0;
        $bestTime=0;
        $i=0;
        foreach ($this->cars as $car){
          $timeOnTrack=(($this->lapLength*$this->lapsNumber)/$car->getSpeed())*60*60+ceil((($this->lapLength*$this->lapsNumber/100)*
          $car->getFuelConsumption())/$car->getFuelTankVolume())*$car->getPitStopTime();
          if ($timeOnTrack<$bestTime || $bestTime==0 ){
            $bestTime=$timeOnTrack;
            $bestCarIndex=$i;
          }
          $i++;
        }
        return $this->cars[$bestCarIndex];
    }
}
