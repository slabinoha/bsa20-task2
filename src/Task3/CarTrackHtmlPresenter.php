<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $presentation='';
        foreach ($track->all() as $car) {
          $presentation.='<div>
                            <p>/'.$car->getName().': '.$car->getPitStopTime().', '.$car->getFuelTankVolume().'/</p>
                            <img src="'.$car->getImage().'">
                          </div>';
        }
        return $presentation;
    }
}
